package com.example.hpyno

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.example.vicky.sqliteexample.DataBaseHandler
import com.jjoe64.graphview.GraphView
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import kotlinx.android.synthetic.main.activity_gps.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.lang.Float.parseFloat
import java.lang.Integer.parseInt
import java.util.*

class GpsJavaActivity : AppCompatActivity(), LocationListener, SensorEventListener {



    var odValue:Int=0
    var doValue:Int=0
    var masa:Int=0
    var koz:Float= 0.0F
    var rpm:Int=0

    var currentSpeed = 0.0
    var testFinished = false

    var timerStart: Double? = null
    var timerEnd:Double? = 0.0
    val arrayBrzine = LinkedList<Float>()
    val arrayVremena = LinkedList<Int>()
    val arrayUbrzanja = LinkedList<Float>()
    var gearRatio=0F
    public val accelometerReading = FloatArray(3)

    lateinit var sensorManager: SensorManager



    public override fun onCreate(saveInstanceState: Bundle?) {
        super.onCreate(saveInstanceState)
        setContentView(R.layout.activity_gps)

        val bundle: Bundle? = intent.extras
        val OdValue = intent.getStringExtra("OdValue")
        val DoValue = intent.getStringExtra("DoValue")
        val MasaEditText = intent.getStringExtra("MasaEditText")
        val KozEditText = intent.getStringExtra("KozEditText")
        val Rpm = intent.getStringExtra("startRPM")

        textDoGps.text = "/"+DoValue
        odValue = parseInt(OdValue)
        doValue = parseInt(DoValue)
        masa = parseInt(MasaEditText)
        koz = parseFloat(KozEditText)
        rpm = parseInt(Rpm)
        gearRatio = rpm.toFloat()/(odValue.toFloat()/3.6f)


        sensorManager = getSystemService(Context.SENSOR_SERVICE)as SensorManager
        sensorManager.registerListener(this,sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),SensorManager.SENSOR_DELAY_NORMAL)

        GlobalScope.launch { makeArrayOfAcceleration() }

        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
            1
        )
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            //No permission
        } else {
            doStuff()
        }


        /*
        testFinished = true
        arrayBrzine.add(20f)
        arrayBrzine.add(40f)
        arrayVremena.add(0)
        arrayVremena.add(6000)

        var g = 0
        while(g<600)
        {
            arrayUbrzanja.add(1.4f)
            g++
        }

        zavrsiTest()
        */

    }


    suspend fun makeArrayOfAcceleration(){
        while(testFinished==false)
        {
            if(currentSpeed>odValue)
            {
                arrayUbrzanja.add(accelometerReading[1])
            }
            delay(10)
        }
    }

    override fun onLocationChanged(location: Location) {
        val txt = findViewById<View>(R.id.speedID) as TextView
        if (location == null) {
            txt.text = "-.- km/h"
        } else {
            if(testFinished==false)
            {
            val nCurrentSpeed = location.speed * 3.6f
            if (nCurrentSpeed > odValue && timerStart==null)
            {
                timerStart=System.currentTimeMillis().toDouble()

            }
            if(nCurrentSpeed > odValue)
            {
                arrayBrzine.add(location.speed)
                currentSpeed = nCurrentSpeed.toDouble()
                arrayVremena.add(System.currentTimeMillis().toInt())
            }
            if(nCurrentSpeed > doValue)
            {
                testFinished = true
                arrayBrzine.add(location.speed)
                arrayVremena.add(System.currentTimeMillis().toInt())
                timerEnd = System.currentTimeMillis().toDouble()
                zavrsiTest()
            }
            txt.text = nCurrentSpeed.toInt().toString()
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1000) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                doStuff()
            } else {
                finish()
            }
        }
    }

    private fun doStuff() {
        val lm = this.getSystemService(LOCATION_SERVICE) as LocationManager
        if (lm != null) {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {

                return
            }
            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0f, this)

        }
        Toast.makeText(this, "Waiting for GPS connection!", Toast.LENGTH_SHORT).show()
    }

    override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
    override fun onProviderEnabled(provider: String) {}
    override fun onProviderDisabled(provider: String) {}


    fun zavrsiTest()
    {
        var timeArray = makeArrayInt(arrayVremena)
        var speedArray = makeArrayFloat(arrayBrzine)

        var rawAccelerationArray =  makeArrayFloat(arrayUbrzanja)

        var loopNumber = rawAccelerationArray.size
        var newArrayNum = 0
        while(loopNumber>0)
        {
            newArrayNum++
            loopNumber-=5
        }

        var accelerationArray = Array<Float>(newArrayNum){i->0f}

        var l = newArrayNum-1

        loopNumber = rawAccelerationArray.size-1
        while(loopNumber>=0)
        {
            if(loopNumber<4)
            {
                var k = loopNumber
                var sumForK = 0f
                while(k>=0)
                {
                    sumForK=sumForK+rawAccelerationArray[k].toFloat()
                    k--
                }
                accelerationArray[l]=sumForK/(loopNumber.toFloat()+1f)
                l--
                loopNumber-=5
            }
            else
            {
                var averageRaw = (rawAccelerationArray[loopNumber].toFloat() + rawAccelerationArray[loopNumber-1].toFloat() + rawAccelerationArray[loopNumber-2].toFloat() + rawAccelerationArray[loopNumber-3].toFloat() + rawAccelerationArray[loopNumber-4].toFloat())/5f


                accelerationArray[l]=averageRaw
                l--
                loopNumber-=5
            }
        }


        var power = Array<Double>(accelerationArray.size){i->0.0}
        var revsPerSec = Array<Double>(accelerationArray.size){i->0.0}

        var testTimeD : Double = (timeArray[timeArray.size-1].minus(timeArray[0]))/1000
        var testTime : Float = testTimeD.toFloat()

        var startSpeed : Float= speedArray[0].toFloat()
        var endSpeed : Float = speedArray[speedArray.size-1].toFloat()



        var j = 0
        var accelerationSum : Float= 0.0f
        while(j<accelerationArray.size)
        {
            accelerationSum = (accelerationSum + accelerationArray[j]).toFloat()
            j = j + 1
        }

        var averageAcceleration : Float = accelerationSum/ accelerationArray.size!!

        var averageGpsAccleration : Float = (endSpeed-startSpeed)/testTime

        var doSumOrDiv = 0
        var accelerationDiff : Float = 0.0f
        if(averageAcceleration>averageGpsAccleration)
        {
           accelerationDiff = (averageAcceleration-averageGpsAccleration).toFloat()
        }
        else
        {
            accelerationDiff = (averageGpsAccleration.toFloat()-averageAcceleration).toFloat()
            doSumOrDiv = 1
        }
        j = 0
        while (j<accelerationArray.size)
        {
            if(doSumOrDiv==0)
            {
                accelerationArray[j]=accelerationArray[j]-accelerationDiff
                if(accelerationArray[j]<0f)
                {
                    accelerationArray[j]==0.01f
                }
            }
            else
            {
                accelerationArray[j]=accelerationArray[j]+accelerationDiff
            }
            j = j + 1
        }

        var x = 0

        var timeStep = testTime/accelerationArray.size

        var speedInLoop : Float = startSpeed

        while(x<accelerationArray.size)
        {

            var acceleration : Float = accelerationArray[x].toFloat()
            speedInLoop = speedInLoop + acceleration*timeStep
            var averageSpeed : Float = speedInLoop
            var utrsenoVrijeme : Float = timeStep
            var silaAkceleracije : Float = masa*acceleration
            var silaOtporaZraka : Float = (koz*1.1f*averageSpeed*averageSpeed)/2
            var ukupnaSlia : Float = silaAkceleracije+silaOtporaZraka
            var prijedeniPut : Float = ((averageSpeed*utrsenoVrijeme) + (acceleration*utrsenoVrijeme*utrsenoVrijeme)/2)
            var snaga = (((ukupnaSlia*prijedeniPut)/utrsenoVrijeme)/0.745f)/1000f
            var revs = averageSpeed*gearRatio

            power[x]=snaga.toDouble()
            revsPerSec[x]=revs.toDouble()

            x = x + 1
        }

        var powerSize = power.size
        var powerNum = 0
        while(powerSize>0)
        {
            powerNum++
            powerSize-=3
        }


        var newPowerArray = Array<Double>(powerNum){i->0.0}
        var newRevArray = Array<Double>(powerNum){i->0.0}
        var a = powerNum-1
        var b = power.size-1
        while(b>=0)
        {
            if(b<2)
            {
                var c = b
                var sumForNewPow = 0.0
                var sumForNewRev = 0.0
                while(c>=0)
                {
                    sumForNewPow = sumForNewPow + power[c]
                    sumForNewRev = sumForNewRev + revsPerSec[c]
                    c--
                }
                newPowerArray[a]=sumForNewPow/(b+1).toDouble()
                newRevArray[a]=sumForNewRev/(b+1).toDouble()
                a--
                b-=3
            }
            else
            {
                var averagePow = (power[b]+power[b-1]+power[b-2])/3.0
                var averageRev = (revsPerSec[b]+revsPerSec[b-1]+revsPerSec[b-2])/3.0
                newPowerArray[a]=averagePow
                newRevArray[a] = averageRev
                a--
                b-=3
            }
        }



        var isSorted = false


        while(isSorted==false)
        {
            var i = 0
            while(i<newRevArray.size-1)
            {
                if(newRevArray[i]>newRevArray[i+1])
                {
                    var swapRev = newRevArray[i]
                    newRevArray[i]=newRevArray[i+1]
                    newRevArray[i+1]==swapRev
                    var swapPower = newPowerArray[i]
                    newPowerArray[i]=newPowerArray[i+1]
                    newPowerArray[i+1]=swapPower
                }
                i++
            }
            i=0
            var foundErrors = 0
            while(i<newRevArray.size-1)
            {
                if(newRevArray[i]>newRevArray[i+1])
                {
                    foundErrors++
                }
                i++
            }
            if(foundErrors==0)
            {
                isSorted=true
            }


        }
        var y = 0
        var maxPower = 0.0
        while(y<newPowerArray.size-1)
        {
            if(newPowerArray[y]>maxPower)
            {
                maxPower = newPowerArray[y]
            }
            y++
        }



        var plot: GraphView = findViewById(R.id.plot)

        plot.viewport.isXAxisBoundsManual=true
        plot.viewport.setMaxX(newRevArray[newRevArray.size-1]+500.0)
        plot.viewport.isYAxisBoundsManual=true
        plot.viewport.setMaxY(maxPower+20)


        var series : LineGraphSeries<DataPoint> = LineGraphSeries()
        var i = 0

        while(i<newPowerArray.size)
        {
            series.appendData(DataPoint(newRevArray[i],newPowerArray[i]),true,power.size)
            i++
        }
        plot.addSeries(series)


        var savePower:String = ""
        var saveRevs:String = ""

        var stringLoop = newPowerArray.size
        var e = 0
        while(e<stringLoop)
        {
            savePower = savePower+newPowerArray[e].toString()
            saveRevs = saveRevs+newRevArray[e].toString()

            if(e<stringLoop-1)
            {
                savePower = savePower+","
                saveRevs = saveRevs+","
            }
            e++

        }






        SaveButton.setOnClickListener() {
            var name:String = saveAs.text.toString()
            saveToDb(name,savePower,saveRevs)
        }


    }



    fun saveToDb(name:String,power:String,revs:String)
    {

        var dataForInserting:Run = Run(name,revs,power)
        var databaseHandler:DataBaseHandler = DataBaseHandler(applicationContext)
        databaseHandler.insertData(dataForInserting)

    }

    fun makeArrayFloat(list:LinkedList<Float>): Array<Double> {
        var size = list.size
        var array = Array<Double>(size){i->0.0}
        var i = 0
        for(element in list)
        {
            array[i] = element.toDouble()
            i = i + 1
        }
        return array
    }

    fun makeArrayInt(list:LinkedList<Int>): Array<Double> {
        var size = list.size
        var array = Array<Double>(size){i->0.0}
        var i = 0
        for(element in list)
        {
            array[i] = element.toDouble()
            i = i + 1
        }
        return array
    }

    override fun onSensorChanged(event: SensorEvent?) {
        if(event == null)
        {
            return
        }
        if(event.sensor.type == Sensor.TYPE_ACCELEROMETER)
        {
            System.arraycopy(event.values,0,accelometerReading,0,accelometerReading.size)
        }
    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {
    }

}