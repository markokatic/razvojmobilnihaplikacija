package com.example.hpyno.ui.slideshow

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.content.ContextCompat.startActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import com.example.hpyno.*
import com.example.hpyno.databinding.FragmentSlideshowBinding
import com.example.vicky.sqliteexample.DataBaseHandler
import com.google.android.material.internal.ContextUtils.getActivity

import java.util.*

class SlideshowFragment : Fragment() {

    private var _binding: FragmentSlideshowBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val slideshowViewModel =
            ViewModelProvider(this).get(SlideshowViewModel::class.java)

        _binding = FragmentSlideshowBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val listView = root.findViewById<ListView>(R.id.runListView)
        listView.adapter = MyCustomAdapter(
            requireContext(),
            activity,
            fragmentManager = requireFragmentManager(), this
        )


        slideshowViewModel.text.observe(viewLifecycleOwner) {

        }
        return root
    }
    fun startShowRuns(run:Run)
    {
        val intent = Intent(activity, ShowRuns::class.java)
        intent.putExtra("runName",run.name.toString())
        intent.putExtra("powerString",run.power.toString())
        intent.putExtra("revString",run.rpm.toString())
        activity?.startActivity(intent)
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


    private class MyCustomAdapter(
        context: Context,
        activity: FragmentActivity?,
        fragmentManager: FragmentManager,
        fragment:Fragment): BaseAdapter() {

        private val mContext: Context
        private val activity : FragmentActivity
        private var list : MutableList<Run> = ArrayList()
        private var databaseHandler : DataBaseHandler
        private var fragmentManager:FragmentManager
        private var fragment: Fragment
        init {
            mContext = context;
            this.activity = activity!!
            databaseHandler = DataBaseHandler(context)
            list = databaseHandler.readData()
            this.fragmentManager = fragmentManager
            this.fragment = fragment
        }

        // responsible for how many rows in my list
        override fun getCount(): Int {
            return list.size
        }

        // you can also ignore this
        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        // you can ignore this for now
        override fun getItem(position: Int): Any {
            return "TEST STRING"
        }

        // responsible for rendering out each row
        override fun getView(position: Int, convertView: View?, viewGroup: ViewGroup?): View {
            val layoutInflater = LayoutInflater.from(mContext)
            val rowMain = layoutInflater.inflate(R.layout.row, viewGroup, false)
            val run = list[position]
            val nameTextView = rowMain.findViewById<TextView>(R.id.nameOfRun)
            nameTextView.text = run.name

            val button = rowMain.findViewById<Button>(R.id.buttonView)
            val delButton= rowMain.findViewById<Button>(R.id.deleteButton)
            button.setOnClickListener()
            {
                val intent = Intent(activity, ShowRuns::class.java)
                intent.putExtra("runName",run.name.toString())
                intent.putExtra("powerString",run.power.toString())
                intent.putExtra("revString",run.rpm.toString())
                activity?.startActivity(intent)
            }
            delButton.setOnClickListener()
            {
                databaseHandler.deleteData(run.id)
                fragmentManager.beginTransaction().detach(fragment).attach(fragment).commit()
            }



            return rowMain
        }

    }
}