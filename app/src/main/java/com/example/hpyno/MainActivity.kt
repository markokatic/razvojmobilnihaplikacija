package com.example.hpyno

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.google.android.material.navigation.NavigationView
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.hpyno.databinding.ActivityMainBinding
import com.google.android.gms.common.util.CollectionUtils.setOf
import com.google.android.material.internal.ContextUtils
import kotlinx.android.synthetic.main.activity_gps.*

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.appBarMain.toolbar)

        val drawerLayout: DrawerLayout = binding.drawerLayout
        val navView: NavigationView = binding.navView
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)


        val odEditText = findViewById<EditText>(R.id.odEditText)
        val doEditText = findViewById<EditText>(R.id.doEditText)
        val masaEditText = findViewById<EditText>(R.id.masaEditText)
        val kozEditText = findViewById<EditText>(R.id.kozEditText)
        val startRPM = findViewById<EditText>(R.id.startRPM)
        val buttonClick = findViewById<Button>(R.id.StartButton)
        buttonClick.setOnClickListener {
            val intent = Intent(this, GpsJavaActivity::class.java)
            intent.putExtra("OdValue",odEditText.text.toString())
            intent.putExtra("DoValue",doEditText.text.toString())
            intent.putExtra("MasaEditText",masaEditText.text.toString())
            intent.putExtra("KozEditText",kozEditText.text.toString())
            intent.putExtra("startRPM",startRPM.text.toString())
            if(
                odEditText.text.length>0 &&
                doEditText.text.length>0 &&
                masaEditText.text.length>0 &&
                kozEditText.text.length>0 &&
                startRPM.text.length>0
            )
            {
                startActivity(intent)
            }

        }

    }
    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }





}