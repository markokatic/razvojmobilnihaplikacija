package com.example.hpyno

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.jjoe64.graphview.GraphView
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import java.lang.Double.parseDouble

class ShowRuns: AppCompatActivity() {

    var powerString:String=""
    var revString:String=""
    var runName1:String=""

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.show_runs)
        powerString = intent.getStringExtra("powerString").toString()
        revString = intent.getStringExtra("revString").toString()
        runName1 = intent.getStringExtra("runName").toString()
        val runName = findViewById<TextView>(R.id.runName)
        runName.text = runName1.toString()
        drawPlot()
    }



    fun stringArraySize(input : String): Int {
        var size = input.length-1
        var commas = 0
        while(size>0)
        {
            if(input[size]==',')
            {
                commas++
            }
            size--
        }
        return commas + 1
    }
    fun drawPlot()
    {
        var powerStringSize = stringArraySize(powerString)
        var revStringSize = stringArraySize(revString)

        var power = Array<Double>(powerStringSize){i->0.0}
        var rev = Array<Double>(revStringSize){i->0.0}

        var i = 0
        var x = 0
        var powerValue:String = ""
        while(i<powerString.length)
        {

            if(powerString[i]==','||i==powerString.length-1)
            {
                var double:Double? = parseDouble(powerValue)
                power[x]=double!!
                x++
                powerValue=""
            }
            else
            {
                powerValue = powerValue + powerString[i]
            }

            i++
        }

        i = 0
        x = 0
        var revValue = ""

        while(i<revString.length)
        {
            if(revString[i]==','||i==revString.length-1)
            {
                var double:Double? = parseDouble(revValue)
                rev[x]=double!!
                x++
                revValue=""
            }
            else
            {
                revValue = revValue + revString[i]
            }

            i++
        }

        var plot: GraphView = findViewById(R.id.plot)

        var series : LineGraphSeries<DataPoint> = LineGraphSeries()

        i = 0
        while(i<power.size)
        {
            series.appendData(DataPoint(rev[i],power[i]),true,5000)
            i++
        }
        plot.addSeries(series)

    }

}