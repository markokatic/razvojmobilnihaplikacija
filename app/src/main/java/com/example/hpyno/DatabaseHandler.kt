package com.example.vicky.sqliteexample

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.widget.Toast
import com.example.hpyno.Run
/**
 * Created by VickY on 2017-11-28.
 */

val DATABASE_NAME ="MyDB"
val TABLE_NAME="Runs"
val COL_NAME = "name"
val COL_POWER = "power"
val COL_RPM = "rpm"
val COL_ID = "id"

class DataBaseHandler(var context: Context) : SQLiteOpenHelper(context,DATABASE_NAME,null,1){
    override fun onCreate(db: SQLiteDatabase?) {

        val createTable = "CREATE TABLE " + TABLE_NAME +" (" +
                COL_RPM +" STRING," +
                COL_NAME + " VARCHAR(256)," +
                COL_POWER +" STRING,"+
                COL_ID +" INTEGER  PRIMARY KEY AUTOINCREMENT)"

        db?.execSQL(createTable)

    }

    override fun onUpgrade(db: SQLiteDatabase?,oldVersion: Int,newVersion: Int) {
    }

    fun insertData(run : Run){
        val db = this.writableDatabase
        var cv = ContentValues()
        cv.put(COL_NAME,run.name)
        cv.put(COL_POWER,run.power)
        cv.put(COL_RPM,run.rpm)
        var result = db.insert(TABLE_NAME,null,cv)
        if(result == -1.toLong())
            Toast.makeText(context,"Failed",Toast.LENGTH_SHORT).show()
        else
            Toast.makeText(context,"Success",Toast.LENGTH_SHORT).show()
    }

    @SuppressLint("Range")
    fun readData() : MutableList<Run>{
        var list : MutableList<Run> = ArrayList()

        val db = this.readableDatabase
        val query = "Select * from " + TABLE_NAME
        val result = db.rawQuery(query,null)
        if(result.moveToFirst()){
            do {
                var run = Run()
                run.name = result.getString(result.getColumnIndex(COL_NAME))
                run.power = result.getString(result.getColumnIndex(COL_POWER))
                run.rpm = result.getString(result.getColumnIndex(COL_RPM))
                run.id = result.getInt(result.getColumnIndex(COL_ID))
                list.add(run)
            }while (result.moveToNext())
        }

        result.close()
        db.close()
        return list
    }

    fun deleteData(col_id:Int){
        val db = this.writableDatabase
        val success = db.execSQL("delete from "+ TABLE_NAME+" where "+COL_ID+"="+col_id.toString())
        db.close()
    }


}